<?php
  $dir = "/var/www/html";
  $files = scandir($dir);
  $IGNORE_DIRS = array('.', '..', 'phpmyadmin'); //Note: Directories begining with an '_' is also ignored by default.
  $CURRENT_DIR = basename(__DIR__);
?>
<!doctype html>
<html>
  <head>
    <title>Linux MySQL Apache PHP - Web Manager</title>

    <link rel="stylesheet" href="/<?php print $CURRENT_DIR; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/<?php print $CURRENT_DIR; ?>/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="/<?php print $CURRENT_DIR; ?>/css/lamp.css" />

    <script type="text/javascript" src="/<?php print $CURRENT_DIR; ?>/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/<?php print $CURRENT_DIR; ?>/js/bootstrap.min.js"></script>

  </head>
  <body>
    <div class="container">

      <h1 class="text-center">Linux Apache, MySQL &amp; PHP Home
        <small>Web Manager By: Angsar Doolan</small>
      </h1>

      <div class="row vspace">
        <div class="col-xs-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title"><span class="glyphicon glyphicon-wrench"></span> Tools</h3>
            </div>
            <div class="panel-body tools">
              <div class="list-group">
                  <a href="/<?php print $CURRENT_DIR; ?>/tools/info.php" target="_blank" class="list-group-item">
                    <img src="/<?php print $CURRENT_DIR; ?>/imgs/phpinfo.gif" alt="phpinfo" /> phpinfo()
                  </a>

                  <a href="/phpmyadmin" target="_blank" class="list-group-item">
                    <img src="/phpmyadmin/themes/pmahomme/img/logo_right.png" alt="phpMyAdmin" />
                  </a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-8">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title"><span class="glyphicon glyphicon-cloud"></span> Sites</h3>
            </div>
            <div class="panel-body sites">
              <div class="list-group">
                <?php
                  $has_sites = false;
                  foreach($files as $file){
                    if(is_dir($file) && !in_array($file, $IGNORE_DIRS) && substr($file, 0, 1) !== '_') {
                      $has_sites = true;
                      print '<a href="./' . $file . '" class="list-group-item">'.'
                              <span class="glyphicon glyphicon-folder-open hspace"></span>' . $file . '</a>';
                    }
                  }

                  if(!$has_sites) {
                      print '<div class="list-group-item text-danger">'.
                              '<span class="glyphicon glyphicon-exclamation-sign hspace"></span>'.
                              'You have not created any site directories as yet.'.
                            '</div>';
                  }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- row -->
    </div>
  </body>
</html>
