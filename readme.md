# Linux Apache, MySQL & PHP  - Web Manager
**Developed By:** Angsar Doolan  
This was written simply to easy the navigation between multi-sites, and to be able to quickly select tools like phpMyAdmin.  
It was inspired by the use of WAMP.  

# Setup
1.   Clone or Copy Directory '_lamp_web_mgr' to your server root.  
* Eg: '/var/www/html/'  

2.   Then create a symlink to the 'index.php' from your sever root.  
$> ln -s ./_lamp_web_mgr/index.php index.php
